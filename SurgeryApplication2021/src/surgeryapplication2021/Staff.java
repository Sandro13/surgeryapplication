/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class Staff
        extends Person
        implements Serializable {

    private String ID;
    private String Type; //Option to choose wether the staff is operational(Doctor) or Administration (Receptionist/Pharmacist)
    private String Qualification; //To establish if the staff is a doctor, pharmacist or receptionist

    //Default Constructor
    public Staff() {

        super();

    }

    //Over-ride Constructor
    public Staff(String personName,
            String personSurname,
            int personAge,
            String personGender,
            String staffType,
            String staffID,
            String staffQualification) {

        super(personName,
                personSurname,
                personAge,
                personGender);

        this.ID = staffID;
        this.Type = staffType;
        this.Qualification = staffQualification;

    }

    //-------------------------Get and SET Methods -----------------------------
    /**
     * This method returns the ID of the Staff
     *
     * @return String
     */
    public String getStaffID() {
        return this.ID;
    }

    /**
     * This method sets the ID of the Staff
     *
     * @param staffID String
     */
    public void setstaffID(String staffID) {
        this.ID = staffID;
    }

    public String getStaffType() {
        return this.Type;
    }

    public void setStaffType(String staffType) {
        this.Type = staffType;

    }

    public String getStaffQualification() {
        return this.Qualification;
    }

    public void setStaffQualification(String staffQualification) {
        this.Qualification = staffQualification;

    }

}
