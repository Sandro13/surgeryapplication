/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class DoctorSchedule
        implements Serializable {

    private String schedule_ID;
    private String schedule_doctorID;
    private String schedule_appointmentDate;
    private String schedule_appointmentTimeSlot;
    private boolean schedule_appointmentStatus;

    // Default Constructor
    public DoctorSchedule() {
    }

    /**
     *
     * @param doctorSchedule_ScheduleID
     * @param doctorSchedule_doctorID
     * @param doctorSchedule_AppointmentDate
     * @param doctorSchedule_appointmentTimeSlot
     * @param doctorSchedule_scheduleStatus
     */
    public DoctorSchedule(String doctorSchedule_ScheduleID,
            String doctorSchedule_doctorID,
            String doctorSchedule_AppointmentDate,
            String doctorSchedule_appointmentTimeSlot,
            boolean doctorSchedule_scheduleStatus) {

        this.schedule_ID =
                doctorSchedule_ScheduleID;
        this.schedule_doctorID =
                doctorSchedule_doctorID;
        this.schedule_appointmentDate =
                doctorSchedule_AppointmentDate;
        this.schedule_appointmentTimeSlot =
                doctorSchedule_appointmentTimeSlot;
        this.schedule_appointmentStatus =
                doctorSchedule_scheduleStatus;

    }

    /**
     *
     * @param doctorSchedule_doctorID
     * @param doctorSchedule_AppointmentDate
     * @param doctorSchedule_appointmentTimeSlot
     * @param doctorSchedule_scheduleStatus
     */
    public DoctorSchedule(String doctorSchedule_doctorID,
            String doctorSchedule_AppointmentDate,
            String doctorSchedule_appointmentTimeSlot,
            boolean doctorSchedule_scheduleStatus) {

        this.schedule_doctorID =
                doctorSchedule_doctorID;
        this.schedule_appointmentDate =
                doctorSchedule_AppointmentDate;
        this.schedule_appointmentTimeSlot =
                doctorSchedule_appointmentTimeSlot;
        this.schedule_appointmentStatus =
                doctorSchedule_scheduleStatus;
    }

    //--------------------------GET and SET Methods--------------------------------------------
    /**
     * This method returns the ID of the schedule
     *
     * @return String
     */
    public String getScheduleID() {
        return this.schedule_ID;
    }

    /**
     * This method sets the ID of the schedule
     *
     * @param doctorSchedule_ScheduleID
     */
    public void setScheduleID(String doctorSchedule_ScheduleID) {
        this.schedule_ID =
                doctorSchedule_ScheduleID;
    }

    /**
     * This method returns the ID of the Doctor
     *
     * @return String
     */
    public String getDoctorID() {
        return this.schedule_doctorID;
    }

    /**
     * This method sets the ID of the Doctor
     *
     * @param doctorSchedule_DoctorID
     */
    public void setDoctorID(String doctorSchedule_DoctorID) {
        this.schedule_doctorID =
                doctorSchedule_DoctorID;
    }

    /**
     * This method returns the Date of the appointment
     *
     * @return String
     */
    public String getAppointmentDate() {
        return this.schedule_appointmentDate;
    }

    /**
     * This method sets the Date of the Appointment
     *
     * @param doctorSchedule_AppointmentDate
     */
    public void setDoctorAppointmentDate(String doctorSchedule_AppointmentDate) {
        this.schedule_appointmentDate =
                doctorSchedule_AppointmentDate;
    }

    /**
     * This method returns the TimeSlot of the appointment
     *
     * @return String
     */
    public String getTimeSlot() {
        return this.schedule_appointmentTimeSlot;
    }

    /**
     * This method sets the TimeSlot of the Appointment
     *
     * @param doctorSchedule_AppointmentTimeSlot
     */
    public void setDoctorAppointmentTimeSlot(
            String doctorSchedule_AppointmentTimeSlot) {
        this.schedule_appointmentTimeSlot =
                doctorSchedule_AppointmentTimeSlot;
    }

    /**
     * This method returns the Status of the appointment
     *
     * @return Boolean
     */
    public boolean getStatus() {
        return this.schedule_appointmentStatus;
    }

    /**
     * This method sets the Status of the Appointment
     *
     * @param doctorSchedule_AppointmentStatus
     */
    public void setDoctorAppointmentStatus(
            boolean doctorSchedule_AppointmentStatus) {
        this.schedule_appointmentStatus =
                doctorSchedule_AppointmentStatus;
    }

    //toString Method
    @Override
    public String toString() {

        String viewDoctorScheduleDetails =
                "";
        viewDoctorScheduleDetails =
                "\nSchedule ID : " +
                this.schedule_ID +
                "\nDoctor ID : " +
                this.schedule_doctorID +
                "\nAppointment Date: " +
                this.schedule_appointmentDate +
                "\nAppointment Time: " +
                this.schedule_appointmentTimeSlot +
                "\nStatus : " +
                this.schedule_appointmentStatus;
        return viewDoctorScheduleDetails;
    }

}
