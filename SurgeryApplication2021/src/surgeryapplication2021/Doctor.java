/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class Doctor
        extends Staff
        implements Serializable {

    //private attribute
    private String specialization;

    //Default Constructor
    public Doctor() {

        super();

    }

    /**
     *
     * @param personName
     * @param personSurname
     * @param personAge
     * @param personGender
     * @param staffType
     * @param staffID
     * @param staffQualification
     * @param doctorSpecialization
     */
    public Doctor(String personName,
            String personSurname,
            int personAge,
            String personGender,
            String staffType,
            String staffID,
            String staffQualification,
            String doctorSpecialization) {

        super(personName,
                personSurname,
                personAge,
                personGender,
                staffType,
                staffID,
                staffQualification);
        
        
        this.specialization = doctorSpecialization;

    }

    
    // -------------------- ENCAPSULATION --------------------------------------
    
    
    public String getDoctorSpecialization() {
        return this.specialization;
    }

    public void setDoctorSpecialization(String doctorSpecialization) {
        this.specialization = doctorSpecialization;
    }

}
