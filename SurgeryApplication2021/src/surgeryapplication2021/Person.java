/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class Person
        implements Serializable {

    //The Person's name
    private String name;

    //The Person's surname    
    private String surname;

    //Person's age
    private int age;

    //Person's gender
    private String gender;

    //Default constructor
    public Person() {

    }

    //over-ride constructor
    /**
     * In the over-ride constructor all the parameters will be passed and for
     * any instance of the Class Person all the required parameters need to be
     * passed
     *
     * @param personName
     * @param personSurname
     * @param personAge
     * @param personGender
     */
    public Person(String personName,
            String personSurname,
            int personAge,
            String personGender) {

        this.name =
                personName;
        this.surname =
                personSurname;
        this.age =
                personAge;
        this.gender =
                personGender;

    }

    //-------------------------Get Set Methods (ENCAPSULATION)-------------------
    /**
     * This method returns the name of the Person
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * This method sets the Name of the Person
     *
     * @param name
     */
    public void setName(String name) {
        this.name =
                name;
    }

    /**
     * This method returns the surname of the Person
     *
     * @return String surname
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * This method sets the Surname of the Person
     *
     * @param surname
     */
    public void setSurame(String surname) {
        this.surname =
                surname;
    }

    /**
     * This method returns the age of the Person
     *
     * @return int age
     */
    public int getAge() {
        return this.age;
    }

    /**
     * This method sets the age of the Person
     *
     * @param age
     */
    public void setage(int age) {
        this.age =
                age;
    }

    /**
     * This method returns the gender of the Person
     *
     * @return String gender
     */
    public String getGender() {
        return this.gender;
    }

    /**
     * This method sets the Gender of the Person
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender =
                gender;
    }

    //toString Method
    public String toString() {

        String viewPersonDetails =
                "";
        viewPersonDetails =
                "\nName: " +
                this.name +
                "\nSurname: " +
                this.surname +
                "\nAge: " +
                this.age +
                "\nGender: " +
                this.gender;
        return viewPersonDetails;
    }

}
