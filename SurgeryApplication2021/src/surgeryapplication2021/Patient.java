/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt  to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sandr
 */
public class Patient
        extends Person
        implements Serializable {

    //Private Attributes
    private String ID;
    private Date DOB;
    private String prescriptionHistory;
    private String Allergies;
    private String SpecialRequests;

    //Default Constructor inheriting from Parent Class the default Constructor
    public Patient() {

        super();
    }

    public Patient(String patientID,
            String personName,
            String personSurname,
            int personAge,
            String personGender,
            Date patientDOB,
            String patientPrescriptionHistory,
            String patientAllergies,
            String patientSpecialRequests) {
        super(personName,
                personSurname,
                personAge,
                personGender);

        this.ID =
                patientID;
        this.DOB =
                patientDOB;
        this.prescriptionHistory =
                patientPrescriptionHistory;
        this.Allergies =
                patientAllergies;
        this.SpecialRequests =
                patientSpecialRequests;

    }

    //------------------------------GET and SET methods (ENCAPSULATION)--------
    /**
     *
     * @return
     */
    public String getPatientID() {

        return this.ID;

    }

    public void setPatientID(String patient_ID) {
        this.ID =
                patient_ID;
    }

    public Date getPatientDOB() {

        return this.DOB;

    }

    public void setPatientDOB(Date patient_DOB) {
        this.DOB =
                patient_DOB;
    }

    public String getPatientPrescriptionHistory() {

        return this.prescriptionHistory;

    }

    public void setPatientPrescriptionHistory(String patient_PrescriptionHistory) {
        this.prescriptionHistory =
                patient_PrescriptionHistory;
    }

    public String getPatientAllergies() {

        return this.Allergies;

    }

    public void setPatientAllergies(String patient_Allergies) {
        this.Allergies =
                patient_Allergies;

    }

    public String getPatientSpecialRequests() {

        return this.SpecialRequests;

    }

    public void setPatientSpecialRequests(String patient_SpecialRequests) {
        this.SpecialRequests =
                patient_SpecialRequests;

    }

}
