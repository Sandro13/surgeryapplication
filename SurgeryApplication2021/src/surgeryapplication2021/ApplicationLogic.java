/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;
import java.util.Iterator;
import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author sandr
 */
public class ApplicationLogic
        implements Serializable {

    //Decleration and Initialization of Patient Hashmap
    private static Map<String, Patient> myPatient =
            new HashMap<String, Patient>();
    //Decleration and Initialization of Staff Hashmap
    private static Map<String, Staff> myStaff =
            new HashMap<String, Staff>();
    //Decleration and Initialization of Doctor Hashmap
    private static Map<String, Doctor> myDoctor =
            new HashMap<String, Doctor>();

    //Declaration and initialization of Appointment Vector
    public static Vector myAppointmentVector =
            new Vector();
    //Declaration and initialization of DoctorSchedule Vector
    public static Vector myDoctorScheduleVector =
            new Vector();
    //Declaration and initialization of MedicinePrescription Vector
    public static Vector myMedicineVector =
            new Vector();

    //Decleration of File to contain Patient Objects
    String patientFileName =
            "Patients.obj";
    //Decleration of File to contain Staff Objects
    String staffFileName =
            "Staff.obj";
    //Decleration of File to contain Doctor Objects
    String doctorFileName =
            "Doctors.obj";
    //Decleration of File to contain DoctorSchedule Objects
    String doctorScheduleFileNameVector =
            "DoctorSchedule.obj";
    //Decleration of File to contain Appointments Objects
    String appointmentFileNameVector =
            "Appointments.obj";
    //Decleration of File to contain Medicine Objects
    String medicineFileNameVector =
            "Medicine.obj";

    //Default constructor
    public ApplicationLogic() {

        boolean loadedPatientHashMap =
                loadFromDisk_Patient(patientFileName);
        if (loadedPatientHashMap ==
                false) {
            addPatientRecord("1",
                    "Sandro",
                    "Mizzi",
                    43,
                    "Male",
                    null,
                    "NA",
                    "NA",
                    "No Sepecial Requests");
        }

        boolean loadedAppointmentVector =
                loadFromDisk_Appointment(
                        appointmentFileNameVector);
        if (loadedAppointmentVector ==
                false) {
            addAppointmentVector("1-23/10/2021",
                    "1",
                    "2",
                    "23/10/2021",
                    "09:00AM-09:30AM",
                    "Fever",
                    "",
                    "",
                    "Submitted",
                    "Pending");
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_Patient(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myPatient); //Saving the Hashmap myPatients with Patients Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the Patients Data into ObjectInputStream. If the
     * file is not found exception error is thrown stating that the file does
     * not exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_Patient(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myPatient =
                        (HashMap<String, Patient>) in.readObject();//Reading the Hashmap myPatients with Patients Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_Staff(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myStaff); //Saving the Hashmap myStaff with Staff Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the Staff Data into ObjectInputStream. If the file
     * is not found exception error is thrown stating that the file does not
     * exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_Staff(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myStaff =
                        (HashMap<String, Staff>) in.readObject();//Reading the Hashmap myStaff with Staff Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error while Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_Doctor(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myDoctor); //Saving the Hashmap myDoctor with Doctors Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the Doctors Data into ObjectInputStream. If the
     * file is not found exception error is thrown stating that the file does
     * not exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_Doctor(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myDoctor =
                        (HashMap<String, Doctor>) in.readObject();//Reading the Hashmap myDoctor with Doctors Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_Appointment(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myAppointmentVector); //Saving the Vector MyAppointmentVector with Appointment Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the Appointments Data into ObjectInputStream. If
     * the file is not found exception error is thrown stating that the file
     * does not exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_Appointment(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myAppointmentVector =
                        (Vector) in.readObject();//Reading the Vector myAppointmentVector with Appointments Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_DoctorSchedule(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myDoctorScheduleVector); //Saving the Vector MyDoctorScheduleVector with DoctorSchedule Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the DoctorSchedule Data into ObjectInputStream. If
     * the file is not found exception error is thrown stating that the file
     * does not exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_DoctorScedule(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myDoctorScheduleVector =
                        (Vector) in.readObject();//Reading the Hashmap myDoctorScheduleVector with DoctorSchedule Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (IOException |
                    ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * This method saves the data into an ObjectOutputStream. If the file is not
     * found, an exception error is shown to the programmer stating that the
     * file is not found
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean saveToDisk_Medicine(String path) {
        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(
                            new FileOutputStream(path));//Open Stream
            out.writeObject(myMedicineVector); //Saving the Vector MyMedicineVector with Medicine Objects
            out.close(); //Close Stream <--MANDATORY. If not closed, nothing will be saved
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,
                    "ERROR:" +
                    ioe.getMessage(),
                    "Error while saving",
                    JOptionPane.ERROR_MESSAGE);
            return false;//return not successfull
        }
        return true; //return is successfull
    }

    /**
     * This method will load the Medicine Data into ObjectInputStream. If the
     * file is not found exception error is thrown stating that the file does
     * not exist.
     *
     * @return Boolean - True or False
     * @param path String
     */
    public boolean loadFromDisk_Medicine(String path) {
        if (path !=
                null) {
            try {
                File file =
                        new File(path);
                ObjectInputStream in =
                        new ObjectInputStream(
                                new FileInputStream(file));
                myMedicineVector =
                        (Vector) in.readObject();//Reading the Vector myMedicine with Medicine Objects
                in.close();//Close Stream <--MANDATORY. If not closed, nothing will be saved
                return true;
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(null,
                        "Error Retreiving File:" +
                        fnfe.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            } catch (IOException |
                    ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null,
                        "Error:" +
                        e.getMessage(),
                        "Error ehile Loading",
                        JOptionPane.ERROR_MESSAGE);
                return false; //Unsuccessfull return -LOAD will not Happen 
            }
        } else {
            return false;
        }

    }

    /**
     * We are retrieving an object of type Patient from the HashMap myPatient
     * through a string parameter ID Card
     *
     * @param getPatientData
     * @return
     */
    public static Patient GetPatientData(String getPatientData) {

        Patient patientData =
                myPatient.get(getPatientData);
        return patientData;
    }

    /**
     * this method checks if the Key Name ( Key=ID of Hashmap) already exists in
     * the Hashmap myPatient
     *
     * @param patientID
     * @return
     */
    public static boolean GetPatientID(String patientID) {
        if (myPatient.containsKey(patientID)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * We are retrieving an object of type Staff from the HashMap myPatient
     * through a string parameter ID Card
     *
     * @param getStaffData
     * @return
     */
    public static Staff GetStaffData(String getStaffData) {

        Staff StaffData =
                myStaff.get(getStaffData);
        return StaffData;
    }

    /**
     * this method checks if the Key Name ( Key=ID of Hashmap) already exists in
     * the Hashmap myStaff
     *
     * @param staffID
     * @return
     */
    public static boolean GetStaffID(String staffID) {
        if (myStaff.containsKey(staffID)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * We are retrieving an object of type Staff from the HashMap myPatient
     * through a string parameter ID Card
     *
     * @param getDoctorData
     * @return
     */
    public static Doctor GetDoctorData(String getDoctorData) {

        Doctor doctorData =
                myDoctor.get(getDoctorData);
        return doctorData;
    }

    /**
     * this method checks if the Key Name ( Key=ID of Hashmap) already exists in
     * the Hashmap myStaff
     *
     * @param doctorID
     * @return
     */
    public static boolean GetDoctorID(String doctorID) {
        if (myDoctor.containsKey(doctorID)) {
            return true;
        } else {
            return false;
        }
    }

    public static String[] ListPatientName() {
        String[] split =
                new String[0];
        String p =
                "";

        for (String key
                : myPatient.keySet()) {
            p =
                    key +
                    "\n" +
                    p;
            split =
                    p.split("\n");

        }
        return split;

    }

    public static String[] ListPatientNamebyName(String Id) {
        String[] split =
                new String[0];
        String p =
                Id;
        for (String key
                : myPatient.keySet()) {
            p =
                    key +
                    "\n" +
                    p;
            split =
                    p.split("\n");
        }
        return split;
    }

    public static String[] ListStaffName() {
        String[] split =
                new String[0];
        String p =
                "";

        for (String key
                : myStaff.keySet()) {
            p =
                    key +
                    "\n" +
                    p;
            split =
                    p.split("\n");

        }
        return split;

    }

    public static String[] ListDoctorName() {
        String[] split =
                new String[0];
        String p =
                "";

        for (String key
                : myDoctor.keySet()) {
            p =
                    key +
                    "\n" +
                    p;
            split =
                    p.split("\n");

        }
        return split;

    }

    public void modifyDoctorScheduleVector(int record) {

        myDoctorScheduleVector.removeElement(record);
        saveToDisk_DoctorSchedule(doctorScheduleFileNameVector);
    }

    public void modifyAppointmentVector(int record) {

        myAppointmentVector.removeElement(record);
        saveToDisk_Appointment(appointmentFileNameVector);
    }

    public void modifyMedicineVector(int record) {

        myMedicineVector.removeElement(record);
        saveToDisk_Medicine(medicineFileNameVector);
    }

    /**
     * Add patient record to the HashMap myPatient and save it to disk
     *
     * @param patientID
     * @param patientName
     * @param patientSurname
     * @param patientAge
     * @param patientGender
     * @param patientDOB
     * @param patientPrescriptionHistory
     * @param patientAllergies
     * @param patientSpecialRequests
     */
    public void addPatientRecord(String patientID,
            String patientName,
            String patientSurname,
            int patientAge,
            String patientGender,
            Date patientDOB,
            String patientPrescriptionHistory,
            String patientAllergies,
            String patientSpecialRequests) {
        myPatient.put(patientID,
                new Patient(patientID,
                        patientName,
                        patientSurname,
                        patientAge,
                        patientGender,
                        patientDOB,
                        patientPrescriptionHistory,
                        patientAllergies,
                        patientSpecialRequests));

        //Calling the saveToDisk_Patient method to save the new PatientObject in the file passed through parameters
        saveToDisk_Patient(patientFileName);

    }

    public void editPatientRecord(String patientID,
            String patientName,
            String patientSurname,
            int patientAge,
            String patientGender,
            Date patientDOB,
            String patientPrescriptionHistory,
            String patientAllergies,
            String patientSpecialRequests) {
        myPatient.put(patientID,
                new Patient(patientID,
                        patientName,
                        patientSurname,
                        patientAge,
                        patientGender,
                        patientDOB,
                        patientPrescriptionHistory,
                        patientAllergies,
                        patientSpecialRequests));

        //Calling the saveToDisk_Patient method to save the new PatientObject in the file passed through parameters
        saveToDisk_Patient(patientFileName);

    }

    /**
     *
     * @param staffName
     * @param staffSurname
     * @param staffAge
     * @param staffGender
     * @param staffType
     * @param staffID
     * @param staffQualification
     */
    public void addStaffRecord(String staffName,
            String staffSurname,
            int staffAge,
            String staffGender,
            String staffType,
            String staffID,
            String staffQualification) {
        myStaff.put(staffID,
                new Staff(staffName,
                        staffSurname,
                        staffAge,
                        staffGender,
                        staffType,
                        staffID,
                        staffQualification));

        //Calling the saveToDisk_Staff method to save the new StaffObject in the file passed through parameters
        saveToDisk_Staff(staffFileName);

    }

    /**
     *
     * @param staffName
     * @param staffSurname
     * @param staffAge
     * @param staffGender
     * @param staffType
     * @param staffID
     * @param staffQualification
     */
    public void editStaffRecord(String staffName,
            String staffSurname,
            int staffAge,
            String staffGender,
            String staffType,
            String staffID,
            String staffQualification) {
        myStaff.put(staffID,
                new Staff(staffName,
                        staffSurname,
                        staffAge,
                        staffGender,
                        staffType,
                        staffID,
                        staffQualification));

        //Calling the saveToDisk_Staff method to save the new StaffObject in the file passed through parameters
        saveToDisk_Staff(staffFileName);

    }

    public void addDoctorRecord(String doctorName,
            String doctorSurname,
            int doctorAge,
            String doctorGender,
            String doctorType,
            String doctorID,
            String doctorQualification,
            String doctorSpecialization) {
        myDoctor.put(doctorID,
                new Doctor(doctorName,
                        doctorSurname,
                        doctorAge,
                        doctorGender,
                        doctorType,
                        doctorID,
                        doctorQualification,
                        doctorSpecialization));

        //Calling the saveToDisk_Doctor method to save the new DoctorObject in the file passed through parameters
        saveToDisk_Doctor(doctorFileName);

    }

    /**
     *
     * @param doctorName
     * @param doctorSurname
     * @param doctorAge
     * @param doctorGender
     * @param doctorType
     * @param doctorID
     * @param doctorQualification
     * @param doctorSpecialization
     */
    public void editDoctorRecord(String doctorName,
            String doctorSurname,
            int doctorAge,
            String doctorGender,
            String doctorType,
            String doctorID,
            String doctorQualification,
            String doctorSpecialization) {
        myDoctor.put(doctorID,
                new Doctor(doctorName,
                        doctorSurname,
                        doctorAge,
                        doctorGender,
                        doctorType,
                        doctorID,
                        doctorQualification,
                        doctorSpecialization));

        //Calling the saveToDisk_Doctor method to save the new DoctorObject in the file passed through parameters
        saveToDisk_Doctor(doctorFileName);

    }

    public void addAppointmentVector(String appointment_AppointmentID,
            String appointment_PatientID,
            String appointment_DoctorID,
            String appointment_Date,
            String appointment_TimeSlot,
            String appointment_PatientSymptoms,
            String appointment_DoctorNotes,
            String appointment_Medications,
            String appointment_AppointmentStatus,
            String appointment_AttendanceStatus) {

        myAppointmentVector.add(new Appointment(appointment_AppointmentID,
                appointment_PatientID,
                appointment_DoctorID,
                appointment_Date,
                appointment_TimeSlot,
                appointment_PatientSymptoms,
                appointment_DoctorNotes,
                appointment_Medications,
                appointment_AppointmentStatus,
                appointment_AttendanceStatus));
        saveToDisk_Appointment(appointmentFileNameVector);
    }

    public void editAppointmentVector(String appointment_AppointmentID,
            String appointment_PatientID,
            String appointment_DoctorID,
            String appointment_Date,
            String appointment_TimeSlot,
            String appointment_PatientSymptoms,
            String appointment_DoctorNotes,
            String appointment_Medications,
            String appointment_AppointmentStatus,
            String appointment_AttendanceStatus) {

        myAppointmentVector.add(new Appointment(appointment_AppointmentID,
                appointment_PatientID,
                appointment_DoctorID,
                appointment_Date,
                appointment_TimeSlot,
                appointment_PatientSymptoms,
                appointment_DoctorNotes,
                appointment_Medications,
                appointment_AppointmentStatus,
                appointment_AttendanceStatus));
        saveToDisk_Appointment(appointmentFileNameVector);
    }

    
    
    public void addDoctorScheduleVector(String doctorSchedule_ScheduleID,
            String doctorSchedule_doctorID,
            String doctorSchedule_AppointmentDate,
            String doctorSchedule_appointmentTimeSlot,
            boolean doctorSchedule_scheduleStatus) {

        myDoctorScheduleVector.add(new DoctorSchedule(doctorSchedule_ScheduleID,
            doctorSchedule_doctorID,
            doctorSchedule_AppointmentDate,
            doctorSchedule_appointmentTimeSlot,
            doctorSchedule_scheduleStatus));
        saveToDisk_DoctorSchedule(doctorScheduleFileNameVector);
    }

    public void editDoctorScheduleVector(String doctorSchedule_ScheduleID,
            String doctorSchedule_doctorID,
            String doctorSchedule_AppointmentDate,
            String doctorSchedule_appointmentTimeSlot,
            boolean doctorSchedule_scheduleStatus) {

        myDoctorScheduleVector.add(new DoctorSchedule(doctorSchedule_ScheduleID,
            doctorSchedule_doctorID,
            doctorSchedule_AppointmentDate,
            doctorSchedule_appointmentTimeSlot,
            doctorSchedule_scheduleStatus));
        saveToDisk_DoctorSchedule(doctorScheduleFileNameVector);
    }

      
    public void addMedicineVector(String medicineName,
            String medicineDosage,
            String medicineType) {

        myMedicineVector.add(new MedicinePrescription(medicineName,
            medicineDosage,
            medicineType));
        saveToDisk_Medicine(medicineFileNameVector);
    }

    public void editMedicineVector(String medicineName,
            String medicineDosage,
            String medicineType) {

        myMedicineVector.add(new MedicinePrescription(medicineName,
            medicineDosage,
            medicineType));
        saveToDisk_Medicine(medicineFileNameVector);
    } 
    
    
  }
