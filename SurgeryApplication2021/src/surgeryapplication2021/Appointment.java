/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class Appointment
        implements Serializable {

    //Private Attributes
    String appointment_appointmentID;
    String appointment_patientID;
    String appointment_doctorID;
    String appointment_date;
    String appointment_timeslot;
    String appointment_patientSymptoms;
    String appointment_doctorNotes;
    String appointment_doctorMedications;
    String appointment_appointmentStatus; //Done, Cancelled, Rescheduled
    String appointment_attendanceStatus; //Show, No Show

    /**
     * Default Constructor
     */
    public Appointment() {
    }

    //overloaded Constructor
    /**
     *
     * @param a_AppointmentID
     * @param a_PatientID
     * @param a_DoctorID
     * @param a_Date
     * @param a_TimeSlot
     * @param a_PatientSymptoms
     * @param a_DoctorNotes
     * @param a_Medications
     * @param a_AppointmentStatus
     * @param a_AttendanceStatus
     */
    public Appointment(String a_AppointmentID,
            String a_PatientID,
            String a_DoctorID,
            String a_Date,
            String a_TimeSlot,
            String a_PatientSymptoms,
            String a_DoctorNotes,
            String a_Medications,
            String a_AppointmentStatus,
            String a_AttendanceStatus) {

        this.appointment_appointmentID = a_AppointmentID;
        this.appointment_patientID = a_PatientID;
        this.appointment_doctorID = a_DoctorID;
        this.appointment_date = a_Date;
        this.appointment_timeslot = a_TimeSlot;
        this.appointment_patientSymptoms = a_PatientSymptoms;
        this.appointment_doctorNotes = a_DoctorNotes;
        this.appointment_doctorMedications = a_Medications;
        this.appointment_appointmentStatus = a_AppointmentStatus;
        this.appointment_attendanceStatus = a_AttendanceStatus;

    }

    //----------------------GET and SET Methods --------------------------------
    /**
     * This method returns the ID of the Appointment
     *
     * @return String appointment_appointmentID
     */
    public String getAppointment_AppointmentID() {
        return this.appointment_appointmentID;
    }

    /**
     * This method sets the ID of the Appointment
     *
     * @param app_ID
     */
    public void setAppointment_AppointmentID(String app_ID) {
        this.appointment_appointmentID = app_ID;
    }

    /**
     * This method returns the ID of the Patient
     *
     * @return String appointment_patientID
     */
    public String getAppointment_PatientID() {
        return this.appointment_patientID;
    }

    /**
     * This method sets the ID of the Patient
     *
     * @param app_PatientID
     */
    public void setAppointment_PatientID(String app_PatientID) {
        this.appointment_patientID = app_PatientID;
    }

    /**
     * This method returns the ID of the Doctor
     *
     * @return String appointment_doctorID
     */
    public String getAppointment_DoctorID() {
        return this.appointment_doctorID;
    }

    /**
     * This method sets the ID of the Doctor
     *
     * @param app_DoctorID
     */
    public void setAppointment_DoctorID(String app_DoctorID) {
        this.appointment_doctorID = app_DoctorID;
    }

    /**
     * This method returns the date of the Appointment
     *
     * @return String appointment_date
     */
    public String getAppointment_Date() {
        return this.appointment_date;
    }

    /**
     * This method sets the date of the Appointment
     *
     * @param app_AppointmentDate
     */
    public void setAppointment_Date(String app_AppointmentDate) {
        this.appointment_date = app_AppointmentDate;
    }

    /**
     * This method returns the timeslot of the Appointment
     *
     * @return String appointment_timeslot
     */
    public String getAppointment_Timeslot() {
        return this.appointment_timeslot;
    }

    /**
     * This method sets the timeslot of the Appointment
     *
     * @param app_AppointmentTimeslot
     */
    public void setAppointment_Timeslot(String app_AppointmentTimeslot) {
        this.appointment_timeslot = app_AppointmentTimeslot;
    }

    /**
     * This method returns the symptoms of the Patient
     *
     * @return String appointment_patientSymptoms
     */
    public String getAppointment_patientSymptoms() {
        return this.appointment_patientSymptoms;
    }

    /**
     * This method sets the symptoms of the Patient
     *
     * @param app_AppointmentPatientSymptoms
     */
    public void setAppointment_PatientSymptoms(
            String app_AppointmentPatientSymptoms) {
        this.appointment_patientSymptoms = app_AppointmentPatientSymptoms;
    }

    /**
     * This method returns the Notes of the Doctor
     *
     * @return String appointment_doctorNotes
     */
    public String getAppointment_doctorNotes() {
        return this.appointment_doctorNotes;
    }

    /**
     * This method sets the Notes of the Doctor
     *
     * @param app_Appointment_doctorNotes
     */
    public void setAppointment_doctorNotes(String app_Appointment_doctorNotes) {
        this.appointment_doctorNotes = app_Appointment_doctorNotes;
    }

    /**
     * This method returns the Medications given by Doctor
     *
     * @return String appointment_doctorMedications
     */
    public String getAppointment_doctorMedications() {
        return this.appointment_doctorMedications;
    }

    /**
     * This method sets the Medications given by Doctor
     *
     * @param app_Appointment_doctorMedications
     */
    public void setAppointment_doctorMedications(
            String app_Appointment_doctorMedications) {
        this.appointment_doctorMedications = app_Appointment_doctorMedications;
    }

    /**
     * This method returns the Status of the appointment
     *
     * @return String appointment_appointmentStatus
     */
    public String getAppointment_appointmentStatus() {
        return this.appointment_appointmentStatus;
    }

    /**
     * This method sets the Status of the appointment
     *
     * @param app_Appointment_appointmentStatus
     */
    public void setAppointment_appointmentStatus(
            String app_Appointment_appointmentStatus) {
        this.appointment_appointmentStatus = app_Appointment_appointmentStatus;
    }

    /**
     * This method returns the Status of the attendance
     *
     * @return String appointment_appointmentStatus
     */
    public String getAppointment_attendanceStatus() {
        return this.appointment_attendanceStatus;
    }

    /**
     * This method sets the Status of the attendance
     *
     * @param app_Appointment_attendanceStatus
     */
    public void setAppointment_attendanceStatus(
            String app_Appointment_attendanceStatus) {
        this.appointment_attendanceStatus = app_Appointment_attendanceStatus;
    }

}
