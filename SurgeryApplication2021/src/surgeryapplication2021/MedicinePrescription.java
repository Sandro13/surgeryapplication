/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package surgeryapplication2021;

import java.io.Serializable;

/**
 *
 * @author sandr
 */
public class MedicinePrescription
        implements Serializable {

    private String medicinePrescription_MedicineName;
    private String medicinePrescription_MedicineDosage;
    private String medicinePrescription_MedicineType;

    //Default Constructor
    public MedicinePrescription() {
    }

    //over-ride Constructor
    public MedicinePrescription(String medicineName,
            String medicineDosage,
            String medicineType) {

        this.medicinePrescription_MedicineName = medicineName;
        this.medicinePrescription_MedicineDosage = medicineDosage;
        this.medicinePrescription_MedicineType = medicineType;

    }

    //----------------------GET and SET methods ---------------------------------
    /**
     * This method returns the name of the medicine
     *
     * @return String
     */
    public String getMedicineName() {
        return this.medicinePrescription_MedicineName;
    }

    /**
     * This method sets the name of the medicine
     *
     * @param medicine_name
     */
    public void setMedicineName(String medicine_name) {
        this.medicinePrescription_MedicineName = medicine_name;
    }

    /**
     * This method returns the dosage of the medicine
     *
     * @return String
     */
    public String getMedicineDosage() {
        return this.medicinePrescription_MedicineDosage;
    }

    /**
     * This method sets the dosage of the medicine
     *
     * @param medicine_dosage
     */
    public void setMedicineDosage(String medicine_dosage) {
        this.medicinePrescription_MedicineDosage = medicine_dosage;
    }

    /**
     * This method returns the type of the medicine
     *
     * @return String
     */
    public String getMedicineType() {
        return this.medicinePrescription_MedicineType;
    }

    /**
     * This method sets the type of the medicine
     *
     * @param medicine_type
     */
    public void setMedicineType(String medicine_type) {
        this.medicinePrescription_MedicineType = medicine_type;
    }

}
